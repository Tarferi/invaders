package invaders;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Color;
import java.awt.BorderLayout;

public class ScoreBottom extends JPanel {
	public ScoreBottom() {
		setBackground(new Color(0, 0, 0));
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setPreferredSize(new Dimension(20, 5));
		panel.setBackground(new Color(0xBD295B));

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 0, 0));
		add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new MigLayout("", "[grow][][]", "[50.00]"));

		JLabel lblInvadersLeft = new JLabel("Invaders left:");
		lblInvadersLeft.setForeground(new Color(0, 204, 204));
		panel_1.add(lblInvadersLeft, "cell 1 0,aligny bottom");
		lblInvadersLeft.setFont(new Font("Courier New", Font.BOLD, 24));

		JLabel label = new JLabel("<INVADERSLEFT>");
		label.setForeground(new Color(255, 255, 255));
		label.setFont(new Font("Courier New", Font.BOLD, 24));
		panel_1.add(label, "cell 2 0,aligny bottom");

	}

}
