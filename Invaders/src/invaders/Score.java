package invaders;

import javax.swing.JPanel;

public class Score {

	private int highScore;
	private int credit;
	private int currentScore;

	private JPanel horniPanel;
	private JPanel spodniPanel;

	public Score() {
		highScore = 0;
		credit = 0;
		currentScore = 0;
		horniPanel = new ScoreTop();
		spodniPanel = new ScoreBottom();
	}

	public void updateScore(int addScoreCount) {
		currentScore += addScoreCount;
	}

	public JPanel getTopPanel() {
		return horniPanel;
	}
	public JPanel getBottomPanel() {
		return spodniPanel;
	}
}
