package invaders;

import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;

public class GameBoard extends JFrame {
	private static final long serialVersionUID = -5143817755419129123L;

	public GameBoard() {
		getContentPane().setBackground(new Color(0, 0, 0));
		Score=new Score();
		construct();

	}

	private void construct() {
		getContentPane().setLayout(new MigLayout("", "[grow]", "[43.00][grow][]"));
		JPanel panel_skore = new JPanel();
		getContentPane().add(panel_skore, "cell 0 0,grow");
		panel_skore.setLayout(new BorderLayout(0, 0));
		panel_skore.add(Score.getTopPanel(), BorderLayout.CENTER);

		JPanel panel_hra = new JPanel();
		getContentPane().add(panel_hra, "cell 0 1,grow");
		panel_hra.setLayout(new BorderLayout(0, 0));

		JPanel panel_zahlavi = new JPanel();
		getContentPane().add(panel_zahlavi, "cell 0 2,grow");
		panel_zahlavi.setLayout(new BorderLayout(0, 0));
		panel_zahlavi.add(Score.getBottomPanel(), BorderLayout.CENTER);
	}

	// private List<Player> players;
	private Score Score;

}
