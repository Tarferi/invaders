package invaders;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

public class ScoreTop extends JPanel {
	private static final long serialVersionUID = -14232771084499227L;

	public ScoreTop() {
		setBackground(new Color(0, 0, 0));
		setLayout(new MigLayout("", "[grow][grow][grow]", "[][]"));

		JLabel lblNewLabel = new JLabel("Score < 1 >");
		lblNewLabel.setForeground(new Color(0x39E7EB));
		lblNewLabel.setFont(new Font("Courier New", Font.BOLD, 24));
		add(lblNewLabel, "cell 0 0,alignx center");

		JLabel lblHiScore = new JLabel("HI - SCORE");
		lblHiScore.setForeground(new Color(0x1959FE));
		lblHiScore.setFont(new Font("Courier New", Font.BOLD, 24));
		add(lblHiScore, "cell 1 0,alignx center");

		JLabel lblScore = new JLabel("Level");
		lblScore.setForeground(new Color(0xE1DE3B));
		lblScore.setFont(new Font("Courier New", Font.BOLD, 24));
		add(lblScore, "cell 2 0,alignx center");

		JLabel lblNewLabel_1 = new JLabel("<CURRENTSCORE>");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		add(lblNewLabel_1, "cell 0 1,alignx center");
		lblNewLabel_1.setFont(new Font("Courier New", Font.BOLD, 24));

		JLabel label = new JLabel("<HIGHSCORE>");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Courier New", Font.BOLD, 24));
		add(label, "cell 1 1,alignx center");

		JLabel label_1 = new JLabel("<LEVEL>");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Courier New", Font.BOLD, 24));
		add(label_1, "cell 2 1,alignx center");
	}
}
